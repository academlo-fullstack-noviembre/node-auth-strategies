const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const clientsDB = require('./clients');

//usuario y contraseña
//email y contraseña
passport.use(new LocalStrategy({
    usernameField: "email"
}, async(email, password, done) => {
    try{
        //Comparamos el email con los registros de la DB
        //await Client.findOne({where: {email}})
        const client = await clientsDB.find( 
                clientObj => clientObj.email === email
            );

        if(!client){
            //Failed auth
            return done(null, false);
        }

        const isMatch = client.password === password;

        //Si las contraseñas coinciden
        if(isMatch){
            //Success auth
            return done(null, client);
        }

        done(null, false);
    }catch(error){
        done(error, false);
    }
}));

passport.use(new GoogleStrategy({
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL: "http://localhost:8000/auth/google/callback" 
  }, (accessToken, refreshToken, profile, done) => {
    try{
        console.log(accessToken);
        console.log(refreshToken);
        console.log(profile);
        done(null, profile);
    }catch(error){
        console.log(error);
        done(error, false);
    }
  }
));

passport.serializeUser((user, done) =>{
    return done(null, user);
});

passport.deserializeUser((user, done) => {
    return done(null, user);
})