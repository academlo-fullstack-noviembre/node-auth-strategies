const express = require('express');
const passport = require('passport');
const session = require('express-session');
require('./passport');
const app = express();

const PORT = process.env.PORT || 8000;

//autenticación por email y contraseña
const passportSignIn = passport.authenticate(
    'local', 
    {session: false}
);


const passportGoogle = passport.authenticate('google', {session: true, scope: ['email', 'profile']});

//para poder guardar la sesión del usuario
app.use(session({secret: 'academlo', resave: false, saveUninitizaled: true}))
app.use(express.json());

//Para poder comprobar la sesión del usuario cada vez que haga una petición
app.use(passport.initialize());
app.use(passport.session());

//Ruta con acceso publico
app.get('/', (req, res) => {
    res.send("<h1>Bienvenido</h1><a href='http://localhost:8000/auth/google'>Iniciar sesión con google</a>");
});

app.post('/sign-in', passportSignIn, (req, res) => {
    res.json(req.user);
});

app.get('/auth/google', passportGoogle);

app.get('/auth/google/callback', passport.authenticate('google', {
    successRedirect: '/home',
    failureRedirect: '/auth/google/failure'
}));

//Ruta protegida
app.get('/home', (req, res) => {
    res.send("<h1>"+req.user.displayName+"</h1><img src='"+req.user.photos[0].value+"' />");
});

app.get('/auth/google/failure', (req, res) => {
    res.send("<h1>Unauthorized</h1>");
});

app.listen(PORT, () => {
    console.log("Servidor corriendo sobre el puerto 8000");
})